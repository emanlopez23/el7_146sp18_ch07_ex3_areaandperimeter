
import java.text.NumberFormat;

/**
 *
 * @author el7
 */
/*
7-3 step 2: created class Rectangle
*/
    public class Rectangle {
        private double length;
        private double width;
    /*
    7-3 step 4: added constructor to make width and length both equal to 0
    */
    public Rectangle() {
        length = 0;
        width = 0;
    }//end constructor
    
    /*
    7-3 step 12: overloaded constructor by making second 
    constructor that accepts length and width
    */
    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }//end constructor


    /*
    7-3 step 3: added length and width instances, 
    and then encapsulated them
    */

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }//end method getLength

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }//end method setLength

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }//end method getWidth

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }//end method setWidth
    
    /*
    7-3 step 5: added get method to calculate area and
    return a double
    */
        public double getArea() {
            double area = width * length;
            return area;
    }//end method getArea
        /*
        7-3 step 6: added get method to return area as 
        string. imported java.text.NumberFormat
        */
        public String getAreaString() {
        NumberFormat number = NumberFormat.getNumberInstance();
        number.setMinimumFractionDigits(3);
        String numberFormatted = number.format(this.getArea());
        return numberFormatted;        
    }//end method getAreaString
        
        /*
        7-3 step 7: repeated steps 5 & 6 for perimeter instead of area
        */
        public double getPerimeter() {
            double perimeter = (2 * width) + (2 * length);
            return perimeter;
        }//end method getPerimeter
        
        public String getPerimeterString() {
        NumberFormat number = NumberFormat.getNumberInstance();
        number.setMinimumFractionDigits(3);
        String numberFormatted = number.format(this.getPerimeter());
        return numberFormatted;        
    }//end method getPerimeterString 
    

    
}//end class
