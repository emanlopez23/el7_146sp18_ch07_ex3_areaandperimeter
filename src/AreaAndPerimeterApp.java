import java.util.Scanner;
/*
7-3 step 10: removed import statement for NumberFormat and unnecessary code
*/

public class AreaAndPerimeterApp {

    public static void main(String[] args) {
        System.out.println("Welcome to the Area and Perimeter Calculator");
        System.out.println();

        Scanner sc = new Scanner(System.in);
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            // get input from user
            System.out.print("Enter length: ");
            double length = Double.parseDouble(sc.nextLine());

            System.out.print("Enter width:  ");
            double width = Double.parseDouble(sc.nextLine());
            /*
            7-3 step 8: created rectangle object and set its parameters
            */
            Rectangle rectangle = new Rectangle(length, width);

            // calculate total
            //double area = width * length;
           // double perimeter = 2 * width + 2 * length;
            

            /*
            7-3 step 9: modified code to use methods of rectangle object 
            to get the area and perimeter
            */
            String message = 
                "Area:         " + rectangle.getAreaString() + "\n" +
                "Perimeter:    " + rectangle.getPerimeterString() + "\n";
            System.out.println(message);

            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }//end while loop
        System.out.println("Bye!");
    }//end main method  
}//end class
